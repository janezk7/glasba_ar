﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets
{
    public class MultilanguageString
    {
        public enum Language { Slovenian, English, Croatian, Simlish }; // you can add more if you want
        Dictionary<Language, string> languageMap = new Dictionary<Language, string>();
        static Language UserLanguage = Language.Slovenian;
        public MultilanguageString()
        {

        }

        public MultilanguageString Insert(Language language, string str)
        {
            languageMap[language] = str;
            return this; //for chaining
        }

        public override string ToString()
        {
            if (!languageMap.ContainsKey(UserLanguage))
                return "translation missing";
            return languageMap[UserLanguage];
        }

        public static void SetUserLanguage(Language language)
        {
            UserLanguage = language;
        }
    }
}
