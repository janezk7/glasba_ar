﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Assets;

public class GMaps : MonoBehaviour
{

    public RawImage img;
    public GameObject arCamera;

    public double lat;
    public double lon;

    public int zoom = 14;

    private int _mapWidth = 2880;
    private int _mapHeight = 2880;

    public enum mapType { roadmap, satellite, hybrid, terrain }
    public mapType mapSelected;
    public int scale;

    // user location (GPS) coordinates
    private double _x = 0;
    private double _y = 0;

    private string _gMapsAPIKey = "AIzaSyB305dE5ongluJX_C1uTdgG5ke6yjjhuA8"; /* might stop working at some point! */


    IEnumerator Map()
    {
        //Debug.Log("From Map Coroutine: " + Screen.width + ", " + Screen.height);
        string url = "https://maps.googleapis.com/maps/api/staticmap?center=" + lat + "," + lon +
            "&zoom=" + zoom + "&size=" + _mapWidth + "x" + _mapHeight + "&scale=" + scale +
            "&maptype=" + mapSelected +
            "&markers=color:blue%7Clabel:S%7C40.702147,-74.015794" +
            "&markers=color:green%7Clabel:G%7C40.711614,-74.012318" +
            "&markers=color:red%7Clabel:C%7C40.718217,-73.998284" +
            "&key=" + _gMapsAPIKey;
        WWW www = new WWW(url);
        yield return www;

        img.texture = www.texture;
        /* set map size to stretch through the whole screen */
        img.rectTransform.sizeDelta = new Vector2(_mapWidth, _mapHeight);
    }


    // Use this for initialization
    void Start()
    {
        User.Initialize(arCamera); /* class User needs to be initialized! */

        UpdateUserLocation();
        SetMapSize();
        StartCoroutine(Map());
    }


    // Update is called once per frame
    void Update()
    {
        UpdateUserLocation();
        SetMapSize();
        StartCoroutine(Map());
    }


    private void UpdateUserLocation()
    {
        User.UpdateData(); /* class User is part of namespace Assets (located in Assets/User.cs) */
        _x = User.locationLatLng.x;
        _y = User.locationLatLng.y;

        lat = _x;
        lon = _y;

        //  Debug.Log("User coordinates: " + _x + ", " + _y);
    }


    private void SetMapSize() /* maps width and height have to be equal, otherwise the image gets distorted */
    {
        _mapHeight = Screen.height;
        _mapWidth = Screen.width;

        if (_mapWidth > _mapHeight)
            _mapHeight = _mapWidth;
        else
            _mapWidth = _mapHeight;
    }

}