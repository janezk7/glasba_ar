﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Orientation : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {
        //never sleep
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        //turn on gyro
        Input.gyro.enabled = true;
        //turn on compass
        Input.compass.enabled = true;
        Input.location.Start();
    }

    // Update is called once per frame
    void Update()
    {
        //get gyro info
        Quaternion gyro = Input.gyro.attitude;

        //transform camera rotation to match gyro
        transform.rotation = GyroToUnity(gyro);
        //rotate 90 deg around x axis
        transform.RotateAround(transform.position, new Vector3(1.0f, 0.0f, 0.0f), 90);
        //rotate 180 deg around y axis
        transform.RotateAround(transform.position, new Vector3(0.0f, 1.0f, 0.0f), 180);
    }

    private static Quaternion GyroToUnity(Quaternion q)
    {
        //right-handed coordinates to left-handed coordinates
        return new Quaternion(q.x, q.y, -q.z, -q.w);
    }

    void AutoSceneSwitcher()
    {
        if (Input.deviceOrientation != DeviceOrientation.LandscapeLeft &&
        Input.deviceOrientation != DeviceOrientation.LandscapeRight &&
        Input.deviceOrientation != DeviceOrientation.Portrait)
        {
            // device is not in landscape or portrait mode
            LoadScene("UI_UX_Points_of_Interest");
        }
    }

    void LoadScene(string scene_name)
    {
        SceneManager.LoadScene(scene_name, LoadSceneMode.Additive);
    }
}
