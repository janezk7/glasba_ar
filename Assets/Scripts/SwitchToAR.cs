﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SwitchToAR : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}

    void AutoSceneSwitcher()
    {
        if (Input.deviceOrientation == DeviceOrientation.LandscapeLeft ||
        Input.deviceOrientation == DeviceOrientation.LandscapeRight)
        {
            // device is in landscape mode
            LoadScene("Main"); // 2nd POI interface needed?
        }
        else if (Input.deviceOrientation == DeviceOrientation.Portrait)
        {
            // device is in portrait mode
            LoadScene("Main");
        }
    }

    void LoadScene(string scene_name)
    {

        SceneManager.LoadScene(scene_name, LoadSceneMode.Additive);
    }
}
