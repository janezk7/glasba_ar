﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets
{
    public class PointOfInterest : DynamicGameObject
    {
        public SightData data
        {
            get; set;
        }

        public int categoryIndex
        {
            get; set;
        }

        public void SetDimensionInMeters(float width, float height, float depth)
        {
            Scale = new Vector3(depth, width, height);
        }

        public DistanceBox distanceBox = null;

        public PointOfInterest(GameObject go) : base(go)
        {
            go.SetActive(false);
            distanceBox = new DistanceBox();
            distanceBox.SetParent(go);
            data = new SightData();

            //Add distanceBox plane to dictionary 
            Main.addDistanceBoxToDB(distanceBox.plane, this);
        }

        public float DistanceInMeters(Vector2 latLngPosition)
        {
            double lat1 = latLngPosition.x;
            double lat2 = this.data.coordinates.lat;
            double lon1 = latLngPosition.y;
            double lon2 = this.data.coordinates.lon;

            double theta = (lon1 - lon2) * 0.017453292519943295d; //PI/180 = radian cast
            lat1 = lat1 * 0.017453292519943295d;
            lat2 = lat2 * 0.017453292519943295d;

            return (float)(Math.Acos((Math.Sin(lat1) * Math.Sin(lat2)) + (Math.Cos(lat1) * Math.Cos(lat2) * Math.Cos(theta))) * 6370693.486d); //meters
        }

        public float DistanceInMetersToUser()
        {
            return DistanceInMeters(User.locationLatLng);
        }

        public void MakeActive(bool makeActive = true)
        {
            UnityGameObject.SetActive(makeActive);
        }

        public override void Update()
        {
            var angleToPoi = Utils.DegreeBearing(this.data.coordinates.lat, this.data.coordinates.lon, User.locationLatLng.x,
                User.locationLatLng.y);

            angleToPoi -= User.compassOffset;

            // TODO... Podvajanje kode. Distance se že izračuna v Utils.getNClosestInterests. Lahko damo to kot parameter Update(float poiDistanceToUser)
            float distance = DistanceInMetersToUser();

            //NOTE: notify Aljaž if you change this line
            float renderedDistance = 50.0f; //Mathf.Min(50, distance);

            MakeActive();

            float x = (float)Math.Sin(angleToPoi * Mathf.PI / 180.0d);
            float z = (float)Math.Cos(angleToPoi * Mathf.PI / 180.0d);
            this.Position = User.camera.transform.position + new Vector3(x, 0, z) * renderedDistance;
            this.UnityGameObject.transform.LookAt(User.camera.transform, new Vector3(0, 1, 0));

            distanceBox.setDistanceText((int)distance, data.name, data.address, Main.database.categorySprites[categoryIndex]);
            distanceBox.update();
        }
    }
}
