﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;

public class LoadSights : MonoBehaviour {

    private string sightDataFilename = "jsonZnamenitosti_updated_linki.json";

    private readonly string jsonsResourceFolder = "Sights/";
    private static readonly string images240pResourceFolderPath = "Im_low/";
    private static readonly string images320pResourceFolderPath = "Im_low/";
    private static readonly string images400pResourceFolderPath = "Im/";

    public enum Quality { LOW240p, MED320p, HI400p}

    // List (NOTE: load all) of all sights
    public SightsInfo sights;

    // UI components
    public Image img;
    public Text tb;


	// Use this for initialization
	void Start () {
        LoadJson();
    }

    // Loads one json file - NOTE: load all
    public bool LoadJson()
    {
        string filePath = "";
        if (Application.platform == RuntimePlatform.Android)
        {
            // Android
            string oriPath = Path.Combine(Application.streamingAssetsPath, (jsonsResourceFolder + sightDataFilename));

            // Android only use WWW to read file
            WWW reader = new WWW(oriPath);
            while (!reader.isDone) { }

            string realPath = Application.persistentDataPath + "/json";
            File.WriteAllBytes(realPath, reader.bytes);

            filePath = realPath;
        }
        else
        {
            // iOS
            filePath = Path.Combine(Application.streamingAssetsPath, (jsonsResourceFolder + sightDataFilename));
        }

        if (File.Exists(filePath))
        {
            string jsonData = File.ReadAllText(filePath);
            sights = JsonUtility.FromJson<SightsInfo>(jsonData);
            return true;
        }
        else
        {
            Debug.LogError(filePath + ", not found!");
            Debug.LogError("Persistant:"+ Application.persistentDataPath);
            Debug.LogError("DataPath:" + Application.dataPath);
            Debug.LogError("StreamingAss" + Application.streamingAssetsPath);
            return false;
        }
    }

    
    public static Sprite LoadImage(string fileName, float ppu = 100.0f, Quality quality = Quality.HI400p)
    {
        // Load JPG/PNG from Resources folder
        Sprite spr;
        Texture2D tex;

#pragma warning disable CS0168 // Variable is declared but never used
        byte[] fileData;
#pragma warning restore CS0168 // Variable is declared but never used

#pragma warning disable CS0219 // Variable is assigned but its value is never used
        string filePath = "";
#pragma warning restore CS0219 // Variable is assigned but its value is never used

        string imageFolder = "";

        switch(quality)
        {
            case Quality.LOW240p:
                imageFolder = images240pResourceFolderPath;
                break;
            case Quality.HI400p:
                imageFolder = images400pResourceFolderPath;
                break;
            default:
                imageFolder = images400pResourceFolderPath;
                break;
        }

        // NALAGANJE IZ RESOURCE folderja
        fileName = Path.ChangeExtension(fileName.Trim(), null);
        tex = Resources.Load<Texture2D>(imageFolder + fileName);
        spr = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0, 0), 100.0f);
        return spr;
    }
}
