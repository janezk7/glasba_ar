﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;


public class CloseThisScene : MonoBehaviour, IPointerClickHandler
{
    public string sceneName;
    public string sceneToLoad;
    private ToastMessage toastMessageScript;


    void Awake()
    {
        if (sceneToLoad != "")
        {
            GameObject go = GameObject.Find("Global Settings");
            if (go != null)
            {
                toastMessageScript = go.GetComponent<ToastMessage>();
            }
        }
    }

    void OnEnable()
    {
        // Tell our 'OnLevelFinishedLoading' function to start listening for a scene change as soon as this script is enabled.
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }

    void OnDisable()
    {
        // Tell our 'OnLevelFinishedLoading' function to stop listening for a scene change as soon as this script is disabled.
        SceneManager.sceneLoaded -= OnLevelFinishedLoading;
    }

    public void OnPointerClick(PointerEventData data)
    {
        SceneManager.UnloadSceneAsync(sceneName);
    }

    void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        toastMessageScript.SetToastString("Klikni na zaslon, da zapreš info!");
        toastMessageScript.DisplayToast();
    }
}
