﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using System.IO;

public class ARCatChecker : MonoBehaviour, IPointerClickHandler
{
    public string catName;
    private ARCatStatus status;
    private Texture2D texGray;
    private Texture2D tex;
    private int stevilka = -1;

    void Start()
    {
        status = GameObject.Find("Canvas").GetComponent<ARCatStatus>();
        texGray = Resources.Load<Texture2D>(catName + "Siva");
        tex = Resources.Load<Texture2D>(catName);
        if (catName == "GlasbeneZbirke")
            stevilka = 0;
        else if (catName == "Parki")
            stevilka = 1;
        else if (catName == "Prizorišča")
            stevilka = 2;
        else if (catName == "RojstneHiše")
            stevilka = 3;
    }

    private ARCatChecker aRCatCheckerScript;

    void Awake()
    {
        aRCatCheckerScript = gameObject.GetComponent<ARCatChecker>();
    }

    public void OnPointerClick(PointerEventData data)
    {
        aRCatCheckerScript.ChangeSprite();
    }

    public void ChangeSprite()
    {
        // This will only execute if the objects collider was the first hit by the click's raycast
        status.status[stevilka] = !status.status[stevilka];  // Toggle filter status
        if (status.status[stevilka]) // Set the icon Sprite
        {
            gameObject.GetComponent<Image>().sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0, 0), 100.0f);
        }
        else
        {
            gameObject.GetComponent<Image>().sprite = Sprite.Create(texGray, new Rect(0, 0, texGray.width, texGray.height), new Vector2(0, 0), 100.0f);
        }
    }
}
