﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class OpenSceneAdditive : MonoBehaviour
{
    public string scene_name;
    private Button _btn;

    void Start()
    {
        _btn = this.gameObject.GetComponent<Button>();
        _btn.onClick.AddListener(OpenAdditive);
    }

    // Update is called once per frame
    void OpenAdditive()
    {
        SceneManager.LoadScene(scene_name, LoadSceneMode.Additive);
    }
}
