﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class SelectSight : MonoBehaviour, IPointerClickHandler
{

    public Sprite active_background;
    public Sprite unactive_background;
    public Image background_object;

    private bool isSelected = false;
    private SelectSight selectSightScript;

    // Use this for initialization
    void Start()
    {

    }

    void Awake()
    {
        selectSightScript = gameObject.GetComponent<SelectSight>();
    }

    public void OnPointerClick(PointerEventData data)
    {
        selectSightScript.ChangeSprite();
    }


    public void ChangeSprite()
    {
        if (isSelected)
        {
            isSelected = false;
            background_object.sprite = unactive_background;
        }
        else
        {
            isSelected = true;
            background_object.sprite = active_background;
        }
    }
}
