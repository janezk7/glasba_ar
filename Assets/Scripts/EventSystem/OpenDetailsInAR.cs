﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


public class OpenDetailsInAR : MonoBehaviour, IPointerDownHandler
{
    private Main mainScript;

    void Awake()
    {
        mainScript = GameObject.Find("ARCamera").GetComponent<Main>();
    }
    
    public void OnPointerDown(PointerEventData eventData)
    {
        mainScript.OpenSightDetailsScene();
    }
}
