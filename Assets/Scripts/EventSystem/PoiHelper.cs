﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PoiHelper : MonoBehaviour, IPointerUpHandler
{
    public Assets.PointOfInterest poi;

    // Use this for initialization
    void Start()
    {
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (/*eventData.IsScrolling() || */eventData.dragging)
        {
            //DO NOTHING
        }
        else
            OpenScene();
    }

    void OpenScene()
    {
        Details.title = poi.data.name;
        Details.description = poi.data.description;
        Details.images = poi.data.images.image;
        Details.latitude = poi.data.coordinates.lat;
        Details.longitude = poi.data.coordinates.lon;

        SceneManager.LoadScene("Details", LoadSceneMode.Additive);
    }

}
