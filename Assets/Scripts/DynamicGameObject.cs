﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets
{
    public abstract class DynamicGameObject
    {
        public DynamicGameObject(GameObject gameObject)
        {
            UnityGameObject = gameObject;
            ID = IdGenerator++;
        }

        private static int IdGenerator = 0;
        public int ID { get; private set; }
        public GameObject UnityGameObject { get; private set; }
        public Vector3 Rotation
        {
            get
            {
                return UnityGameObject.transform.eulerAngles;
            }
            set
            {
                UnityGameObject.transform.eulerAngles = value;
            }
        }

        public Vector3 Scale
        {
            get
            {
                return UnityGameObject.transform.localScale;
            }
            set
            {
                UnityGameObject.transform.localScale = value;
            }
        }

        public Vector3 Position
        {
            get
            {
                return UnityGameObject.transform.position;
            }
            set
            {
                UnityGameObject.transform.position = value;
            }
        }

        abstract public void Update();
    }
}
