﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets
{
    public class User
    {
        public static GameObject camera = null;
        public static float compassOffset;
        public static Vector2 locationLatLng;

        public static void Initialize(GameObject camera)
        {
            /* Input.location.Start() method is set to update every 10m by default.
             * This means user needs to move at least 10m to see changes on map.  
             * "10,0.1f" sets accuracy to 0.1m 
             */
            Input.location.Start(10, 0.1f);
            Input.compass.enabled = true;
            User.camera = camera;
            UpdateData();
        }

        public static void UpdateData()
        {
            locationLatLng = new Vector2(Input.location.lastData.latitude, Input.location.lastData.longitude);
        }
    }
}
