﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DebugLogger
{
    static string myLog;
    static Queue myLogQueue = new Queue();

    public static void Invoke()
    {
        Application.logMessageReceived += HandleLog;
    }

    public static void Kill()
    {
        Application.logMessageReceived -= HandleLog;
    }

    private static void HandleLog(string logString, string stackTrace, LogType type)
    {
        myLog = logString;
        string newString = "\n [" + type + "] : " + myLog;
        myLogQueue.Enqueue(newString);
        if (type == LogType.Exception)
        {
            newString = "\n" + stackTrace;
            myLogQueue.Enqueue(newString);
        }
        myLog = string.Empty;
        foreach (string mylog in myLogQueue)
        {
            myLog = myLog + mylog;
        }
        while (myLogQueue.Count > 10)
            myLogQueue.Dequeue();

        Console.WriteLine(logString);
    }

    public static void OnGUIDisplay()
    {
        GUILayout.Label(myLog);
    }
}