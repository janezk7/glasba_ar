﻿using Assets;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Details : MonoBehaviour
{
    public static string title = "YAY";
    public static string description = "text";
    public static List<string> images;
    public static double latitude = 0;
    public static double longitude = 0;


    // Use this for initialization
    void Start()
    {
        Text titleObject = GameObject.Find("title").GetComponent<Text>();
        titleObject.text = title;
        TextMeshProUGUI descObject = GameObject.Find("description").GetComponent<TextMeshProUGUI>();
        description = description.Replace("$$$$", "</color></link>");
        description = description.Replace("$$$", "\"><color=blue>");
        description = description.Replace("$$", "<link=\"");
        descObject.text = description;
        if (images != null && images.Count != 0)
        {
            Image imageObject = GameObject.Find("sight_photo_details").GetComponent<Image>();
            imageObject.sprite = LoadSights.LoadImage(images[1]);
        }

        Button backButton = GameObject.Find("backButton").GetComponent<Button>();
        backButton.onClick.AddListener(BackOnClick);

        Button navigateButton = GameObject.Find("navigateButton").GetComponent<Button>();
        navigateButton.onClick.AddListener(NavigateOnClick);
    }

    void BackOnClick()
    {
        SceneManager.UnloadSceneAsync("Details");
    }

    void NavigateOnClick()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject context = jc.GetStatic<AndroidJavaObject>("currentActivity");

            var uri = new AndroidJavaClass("android.net.Uri").CallStatic<AndroidJavaObject>("parse", "google.navigation:q=" + latitude + "," + longitude);
            AndroidJavaObject intent = new AndroidJavaObject("android.content.Intent", "android.intent.action.VIEW", uri);
            context.Call("startActivity", intent);
        }
        else if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            Application.OpenURL("http://maps.apple.com/?q=" + latitude + "," + longitude);
        }
    }
}
