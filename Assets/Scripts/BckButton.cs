﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BckButton : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //if physical back button is pressed
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            //go though all scenes
            for (int i = 0; i < SceneManager.sceneCount; i++)
            {
                //if only main scene is open, quit app
                if (SceneManager.sceneCount == 1 && SceneManager.GetSceneAt(i).name == "Main")
                {
                    Application.Quit();
                }
                //close info
                if (SceneManager.GetSceneAt(i).name == "Info")
                {
                    SceneManager.UnloadSceneAsync(SceneManager.GetSceneAt(i).name);
                }
                //close info
                if (SceneManager.GetSceneAt(i).name == "Sights")
                {
                    SceneManager.UnloadSceneAsync(SceneManager.GetSceneAt(i).name);
                }
                //close main
                if (SceneManager.GetSceneAt(i).name == "Details")
                {
                    SceneManager.UnloadSceneAsync(SceneManager.GetSceneAt(i).name);
                }
            }
        }
    }
}
