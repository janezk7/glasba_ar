﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Starting in 0.0 seconds.
// Camera will rotate on Z axis every 0.5 seconds

public class BackgroundGlove : MonoBehaviour {

    // Attach this script to the main camera and drag the camera object here
    public Camera main_camera;

    // Use this for initialization
    void Start () {
        InvokeRepeating("RotateCamera", 0.0f, 0.1f);
    }
	
	void RotateCamera() {
        main_camera.transform.Rotate(Vector3.back, 20.0f * Time.deltaTime);
    }
}
