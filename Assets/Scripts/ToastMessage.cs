﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToastMessage : MonoBehaviour {

    private AndroidJavaObject currentActivity;
    private AndroidJavaClass UnityPlayer;
    private AndroidJavaObject context;

    private enum Platform
    {
        WindowsOS, AndroidOS, iOS
    };

    private string toastString;
    private Platform platform = Platform.WindowsOS;

    void Start()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            UnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            currentActivity = UnityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
            context = currentActivity.Call<AndroidJavaObject>("getApplicationContext");

            platform = Platform.AndroidOS;
        }
    }

    public void SetToastString(string s)
    {
        toastString = s;
    }
    
    public void DisplayToast()
    {
        if (platform == Platform.AndroidOS)
        {
            AndroidJavaClass Toast = new AndroidJavaClass("android.widget.Toast");
            AndroidJavaObject javaString = new AndroidJavaObject("java.lang.String", toastString);
            AndroidJavaObject toast = Toast.CallStatic<AndroidJavaObject>("makeText", context, javaString, Toast.GetStatic<int>("LENGTH_LONG"));
            toast.Call("show");
        }
    }
}
