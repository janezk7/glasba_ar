﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Sights : MonoBehaviour
{

    // Prefab konstante
    const int SIGHT_DISTANCE    = 1;
    const int SIGHT_CATEGORY    = 2;
    const int SIGHT_PHOTO       = 3;
    const int SIGHT_TITLE       = 4;
    const int SIGHT_LOCATION    = 5;
    const int SIGHT_REGION      = 6;
    //const int SIGHT_DESCRIPTION = 7;

    // Use this for initialization
    void Start()
    {
        var pois = Main.database.PointsOfInterestsList;
        var contentView = GameObject.Find("Content");
        
        foreach(var poi in pois)
        { 
            var sightTilePrefab = Instantiate(Resources.Load<GameObject>("SightTile"));

            //Pridobitev razdalje
            int distanceMeters = (int)poi.DistanceInMetersToUser();
            string distanceMetersDisplay = distanceMeters + " m";
            if (distanceMeters >= 1000)
                distanceMetersDisplay = (distanceMeters / 1000) + " km";

            Text sight_distance = sightTilePrefab.transform.GetChild(SIGHT_DISTANCE).transform.gameObject.GetComponent<Image>().transform.GetChild(0).transform.gameObject.GetComponent<Text>();
            Image sight_category = sightTilePrefab.transform.GetChild(SIGHT_CATEGORY).transform.gameObject.GetComponent<Image>();
            Image sight_photo = sightTilePrefab.transform.GetChild(SIGHT_PHOTO).transform.gameObject.GetComponent<Image>();
            Text sight_title = sightTilePrefab.transform.GetChild(SIGHT_TITLE).transform.gameObject.GetComponent<Text>();
            Text sight_location = sightTilePrefab.transform.GetChild(SIGHT_LOCATION).transform.gameObject.GetComponent<Text>();
            Text sight_region = sightTilePrefab.transform.GetChild(SIGHT_REGION).transform.gameObject.GetComponent<Text>();
           // Text sight_description = sightTilePrefab.transform.GetChild(SIGHT_DESCRIPTION).transform.gameObject.GetComponent<Text>();

            sight_distance.text = distanceMetersDisplay;
            sight_category.sprite = Main.database.categorySprites[poi.categoryIndex];
            if (poi.data.images.image.Count != 0)
                sight_photo.sprite = LoadSights.LoadImage(poi.data.images.image[1],100,LoadSights.Quality.LOW240p);
            sight_title.text = poi.data.name;
            sight_location.text = "Kraj: \n" + poi.data.city;
            sight_region.text = "Regija: \n" + poi.data.region;
         //   sight_description.text = poi.data.description;

            sightTilePrefab.transform.SetParent(contentView.transform);
            sightTilePrefab.transform.localScale = new Vector3(1, 1, 1);

            sightTilePrefab.GetComponent<PoiHelper>().poi = poi;
        }
    }
}
