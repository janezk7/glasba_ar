﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SightsInfo
{
    public List<SightData> sights;
}

[System.Serializable]
public class SightData
{
    public int id;
    public string name;
    public string address;
    public string city;
    public string region;

    public Coordinates coordinates;

    public string description;
    public string url;

    public Categories categories;

    public Images images;
}

[System.Serializable]
public class Coordinates
{
    public float lat;
    public float lon;
}

[System.Serializable]
public class Categories
{
    public List<string> category;
}

[System.Serializable]
public class Images
{
    public List<string> image;
}
