﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;


public class Main : MonoBehaviour
{
    // Use this for initialization
    float DEBUG_ADD_LAT = 0;
    int nrClosestInterests = 15;

    public static PointsOfInterestsDatabase database;

    void Start()
    {
        //DebugLogger.Invoke(); //-> ODKOMENTIRAJ ZA DEBUG MODE!
        User.Initialize(GameObject.Find("ARCamera"));
        MultilanguageString.SetUserLanguage(MultilanguageString.Language.Slovenian);
        database = new PointsOfInterestsDatabase(this.gameObject);
        database.FetchDataInitialization();
    }

    void OnGUI()
    {
        DebugLogger.OnGUIDisplay();
    }

    // Update is called once per frame
    void Update()
    {
        if (!database.Initialized)
        {
            return;
        }

        DEBUG_ADD_LAT -= 0.000001f;
        UpdatePointsOfInterests();
    }

    private void UpdatePointsOfInterests()
    {
        User.UpdateData();
        bool USE_DEBUG_COORDINATES = false;

        var listOfInterests = database.PointsOfInterestsList;

        var debugCoordinate = new Vector2(46.561760f, 15.648052f);
        if (USE_DEBUG_COORDINATES)
        {
            User.locationLatLng.x = debugCoordinate.x;
            User.locationLatLng.y = debugCoordinate.y;
        }

        // Nastavimo vse POI na inactive
        foreach (var poi in listOfInterests)
        {
            poi.MakeActive(false);
        }

        // Generiramo listo N najbližjih znamenitosti za prikaz
        var listOfClosestsInterests = Utils.getNClosestInterestsWithFilter(listOfInterests, nrClosestInterests);

        //REAL VERTICAL ALIGNMENT CODE
        if (listOfClosestsInterests.Count > 0)
        {
            //create helpers
            var allHelpers = new List<VerticalAlignmentSortingHelper>();
            foreach (var poi in listOfClosestsInterests)
            {
                allHelpers.Add(new VerticalAlignmentSortingHelper(poi));
            }
            //sort helpers by degrees
            allHelpers = allHelpers.OrderBy(p => p.degrees).ToList();
            //maximum allowed distance measured experimentally and scientifically
            //this should be just a big longer than POI GUI size
            //NOTE: maxAllowedDistance needs to be changed if POI GUI size changes
            float maxAllowedDistance = 23.3f;
            //divided all POIs into clusters
            var clusters = new List<List<VerticalAlignmentSortingHelper>>();
            int startIndex = 0;
            //while there are still POIs to be added to clusters, create new clusters
            while (startIndex < allHelpers.Count)
            {
                //create a new cluster by limiting the distance between the first and the last node in a cluster
                //to be the maximum allowed distance
                int i = startIndex + 1;
                while (i < allHelpers.Count &&
                    Vector2.Distance(
                        new Vector2(
                            allHelpers[i].poi.distanceBox.plane.transform.position.x,
                            allHelpers[i].poi.distanceBox.plane.transform.position.z),
                        new Vector2(
                            allHelpers[startIndex].poi.distanceBox.plane.transform.position.x,
                            allHelpers[startIndex].poi.distanceBox.plane.transform.position.z))
                    <= maxAllowedDistance)
                {
                    i++;
                }
                var newCluster = allHelpers.GetRange(startIndex, i - startIndex);
                clusters.Add(newCluster);
                startIndex = i;
            }
            //check if first and last cluster must be combined
            //needs to be done because points with degrees just above 0 and just below 360
            //might be close enough to each other that they should form a single cluster
            if (clusters.Count > 1 && Vector2.Distance(
                new Vector2(
                    clusters.Last().Last().poi.distanceBox.plane.transform.position.x,
                    clusters.Last().Last().poi.distanceBox.plane.transform.position.z),
                new Vector2(
                    clusters.First().First().poi.distanceBox.plane.transform.position.x,
                    clusters.First().First().poi.distanceBox.plane.transform.position.z))
                <= maxAllowedDistance)
            {
                //combine the first and last cluster
                clusters.Last().AddRange(clusters.First());
                clusters.RemoveAt(0);
            }
            //if there is an odd number of clusters this may cause overlapping
            //we must ensure and even number of clusters
            //so we find the two consecutive clusters with the lowest combined number of POIs in them and combine them
            if (clusters.Count > 1 && clusters.Count % 2 != 0)
            {
                var combinedCounts = new List<int>();
                for (int i = 0; i < clusters.Count; i++)
                {
                    if (i == clusters.Count - 1)
                    {
                        combinedCounts.Add(clusters[i].Count + clusters[0].Count);
                    }
                    else
                    {
                        combinedCounts.Add(clusters[i].Count + clusters[i + 1].Count);
                    }
                }
                var lowestCombined = combinedCounts.IndexOf(combinedCounts.Min());
                if (lowestCombined == clusters.Count - 1)
                {
                    clusters[lowestCombined].AddRange(clusters[0]);
                    clusters.RemoveAt(0);
                }
                else
                {
                    clusters[lowestCombined].AddRange(clusters[lowestCombined + 1]);
                    clusters.RemoveAt(lowestCombined + 1);
                }
            }

            //we have the clusters
            //now we align the POIs belonging to the same cluster vertically
            //giving them different Y coordinates in Unity
            //so they don't appear clumped together
            //NOTE: verticalSpacing needs to be changed if POI GUI size changes
            float verticalSpacing = 5.0f;
            //go through all the clusters
            for (int i = 0; i < clusters.Count; i++)
            {
                //calculate number of POIs in cluster to put above and below the horizon
                float currentPosition = 0.0f;
                foreach (var poi in clusters[i])
                {
                    poi.poi.Update();
                    poi.poi.distanceBox.plane.transform.localPosition += new Vector3(0, currentPosition, 0);
                    //odd and even go in different directions (above or below)
                    currentPosition += (i % 2 == 0 ? 1 : -1) * verticalSpacing;
                }
            }
        }
    }

    //vertical alignment sorting by degrees on circle helper class
    class VerticalAlignmentSortingHelper
    {
        //point of interest
        public PointOfInterest poi;
        //degrees between ((reference point in world space) = ((camera) + (1, 0, 0))) and (POI) if (camera) is our center
        public float degrees;

        public VerticalAlignmentSortingHelper(PointOfInterest POI)
        {
            //vect1 = (reference point in world space) - (camera)
            Vector2 vect1 = new Vector2(1, 0);
            //vect2 = (POI) - (camera)
            Vector2 vect2 =
                new Vector2(POI.data.coordinates.lat, POI.data.coordinates.lon) - User.locationLatLng;
            //calculating the angle between vect1 and vect2, we get the desired result
            degrees = Vector2.SignedAngle(vect1, vect2);
            //if angle is negative, we add 360 to always get clockwise angle
            if (degrees < 0.0f)
            {
                degrees += 360.0f;
            }
            //we are going to need the original POI object later to set the vertical alignment
            poi = POI;
        }
    }


    public void OpenSightDetailsScene()
    {
        for (int i = 0; i < Input.touchCount; i++)
        {
            Touch touch = Input.GetTouch(i);
            if (touch.phase != TouchPhase.Began)
            {
                continue;
            }

            RaycastHit raycastInfo;
            bool somethingTouched = Physics.Raycast(Camera.main.ScreenPointToRay(touch.position), out raycastInfo);
            if (!somethingTouched)
            {
                continue;
            }

            PointOfInterest poi;
            if (!database.gameObjectMap.TryGetValue(raycastInfo.collider.gameObject, out poi))
            {
                // Preverjanje trka z DistanceBox plane-om
                if (!database.distanceBoxMap.TryGetValue(raycastInfo.collider.gameObject, out poi))
                {
                    continue;
                }
            }

            Details.title = poi.data.name;
            Details.description = poi.data.description;
            Details.images = poi.data.images.image;
            Details.latitude = poi.data.coordinates.lat;
            Details.longitude = poi.data.coordinates.lon;

            SceneManager.LoadScene("Details", LoadSceneMode.Additive);

            break;
        }
    }

    public static void addDistanceBoxToDB(GameObject plane, PointOfInterest poi)
    {
        database.distanceBoxMap.Add(plane, poi);
    }
}
