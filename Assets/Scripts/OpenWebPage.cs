﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OpenWebPage : MonoBehaviour {

    //Make sure to attach these Buttons in the Inspector
    public Button menu_button;

    void Start()
    {
        Button btn = menu_button.GetComponent<Button>();
        btn.onClick.AddListener(TaskOnClick); // Calls the TaskOnClick method when you click the Button
    }

    void TaskOnClick()
    {
        //Output this to console when the Button is clicked
        Debug.Log("You have clicked the webpage button!");
        Application.OpenURL("https://medijske.um.si/guide2music/"); // TODO: spremenit ko bo novi url
    }
}
