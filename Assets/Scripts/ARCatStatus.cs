﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ARCatStatus : MonoBehaviour
{
    public bool[] status = new bool[] { true, true, true, true };
    
    // Use this for initialization
    void Start()
    {

    }

    public List<int> GetShuffledIndexes()
    {
        //mali hack ker categoryIndex niso v istem vrstnem redu kot zadeve na displayu
        var selectedCategories = new List<int>();
        if (status[0])
            selectedCategories.Add(3);
        if (status[1])
            selectedCategories.Add(0);
        if (status[2])
            selectedCategories.Add(1);
        if (status[3])
            selectedCategories.Add(2);
        return selectedCategories;
    }
}
