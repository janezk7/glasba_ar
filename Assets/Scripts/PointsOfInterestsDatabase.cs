﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Assets
{
    public class PointsOfInterestsDatabase
    {
        string[] categoryNames = { "parki", "prizorišča", "rojstne hiše", "glasbene zbirke" };
        string[] categoryFilenames = { "Parki", "Prizorišča", "RojstneHiše", "GlasbeneZbirke" };
        public List<Sprite> categorySprites;

        public Dictionary<string, PointOfInterest> poiMap = new Dictionary<string, PointOfInterest>();
        public Dictionary<GameObject, PointOfInterest> gameObjectMap = new Dictionary<GameObject, PointOfInterest>(); // Bunkice
        public Dictionary<GameObject, PointOfInterest> distanceBoxMap = new Dictionary<GameObject, PointOfInterest>(); // Distance Boxes (planes)

        private GameObject obj;

        public List<PointOfInterest> PointsOfInterestsList { get; private set; }
        public bool Initialized
        {
            get; private set;
        }
        public PointsOfInterestsDatabase(GameObject _obj)
        {
            obj = _obj;
        }

        public PointOfInterest FindByStringId(string id)
        {
            return poiMap[id];
        }


        public void FetchDataInitialization()
        {
            categorySprites = new List<Sprite>();

            if (Initialized)
            {
                throw new Exception("Already initialized");
            }

            //Naložimo sprite-e kategorij
            foreach (string kat in categoryFilenames)
            {
                Texture2D spriteTexture = Resources.Load<Texture2D>(kat);
                Sprite spr = Sprite.Create(spriteTexture, new Rect(0, 0, spriteTexture.width, spriteTexture.height), new Vector2(0, 0), 100.0f);
                categorySprites.Add(spr);
            }

            // Init start
            string poi_node = "Sphere";
            var poiInstance = new PointOfInterest(UnityEngine.Object.Instantiate(Resources.Load<GameObject>(poi_node)));

            // Fetch sights
            LoadSights loadSights = obj.AddComponent<LoadSights>();
            if (!loadSights.LoadJson())
                throw new Exception("Unable to fetch JSON");
            SightsInfo si = loadSights.sights;

            // Instanciranje POI
            foreach (SightData sd in si.sights)
            {
                // Loads GameObject into the scene
                GameObject blankGO = UnityEngine.Object.Instantiate(Resources.Load<GameObject>(poi_node));
                blankGO.transform.SetParent(GameObject.Find("Sights Container").transform);
                blankGO.GetComponent<Renderer>().enabled = false;
                poiInstance = new PointOfInterest(blankGO);
                poiInstance.data = sd;
                poiInstance.categoryIndex = (sd.categories != null && sd.categories.category.Count > 1) ?
                    Array.IndexOf(categoryNames, sd.categories.category[1]) :
                    Array.IndexOf(categoryNames, 1); // Default kategorija -> Prizorišče

                poiMap.Add(sd.id.ToString(), poiInstance);

                /** //Testiranje kategorij v JSON
                List<string> dejanskeKategorije = new List<string>();
                if (sd.categories.category.Count != 0 && !dejanskeKategorije.Contains(sd.categories.category[1]))
                    dejanskeKategorije.Add(sd.categories.category[1]);
                */
            }

            foreach (var pointOfInterest in poiMap)
            {
                pointOfInterest.Value.UnityGameObject.AddComponent(typeof(BoxCollider));
                gameObjectMap.Add(pointOfInterest.Value.UnityGameObject, pointOfInterest.Value);
            }

            //init end
            PointsOfInterestsList = poiMap.Select(kvp => kvp.Value).ToList();
            Initialized = true;
        }
    }
}
