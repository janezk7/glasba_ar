﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets
{
    public class Utils
    {
        public static double DegreeBearing(
            double lat1, double lon1,
            double lat2, double lon2)
        {
            var dLon = ToRad(lon2 - lon1);
            var dPhi = Math.Log(
                Math.Tan(ToRad(lat2) / 2d + Math.PI / 4d) / Math.Tan(ToRad(lat1) / 2d + Math.PI / 4d));
            if (Math.Abs(dLon) > Math.PI)
                dLon = dLon > 0 ? -(2d * Math.PI - dLon) : (2d * Math.PI + dLon);
            return ToBearing(Math.Atan2(dLon, dPhi));
        }

        public static double ToRad(double degrees)
        {
            return degrees * (Math.PI / 180);
        }

        public static float ToRad(float degrees)
        {
            return (float)(degrees * (Math.PI / 180f));
        }

        public static double ToDegrees(double radians)
        {
            return radians * 180 / Math.PI;
        }

        public static double ToBearing(double radians)
        {
            // convert radians to degrees (as bearing: 0...360)
            return (ToDegrees(radians) + 360) % 360;
        }

        public static List<PointOfInterest> getNClosestInterestsWithFilter(List<PointOfInterest> listOfInterests, int nrClosestInterests)
        {
            Dictionary<PointOfInterest, float> razdalje = new Dictionary<PointOfInterest, float>();
            foreach (var poi in listOfInterests)
            {
                var distance = poi.DistanceInMetersToUser();
                razdalje.Add(poi, distance);
            }

            var statusNumbers = GameObject.Find("Canvas").GetComponent<ARCatStatus>().GetShuffledIndexes();

            var sortedDict = (from entry in razdalje
                              where statusNumbers.Contains(entry.Key.categoryIndex)
                              orderby entry.Value ascending
                              select entry).Take(nrClosestInterests);

            List<PointOfInterest> orderedPOIList = new List<PointOfInterest>(sortedDict.Select(kvp => kvp.Key));
            return orderedPOIList;
        }
    }
}