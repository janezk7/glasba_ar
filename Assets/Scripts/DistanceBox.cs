﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Assets
{
    public class DistanceBox
    {
        // Prefab konstante
        const int SIGHT_ICON = 0;
        const int SIGHT_NAME = 1;
        const int SIGHT_ADDRESS = 2;
        const int SIGHT_DISTANCE = 3;

        // DistanceBox variables
        public GameObject plane;
        GameObject uiTextGO;
        GameObject sightPrefab;
        

        public DistanceBox()
        {
            plane = GameObject.CreatePrimitive(PrimitiveType.Plane);
            plane.transform.localScale = new Vector3(2.3f, 0.1f, 0.39f); // Nastavitev velikosti plane-a da se sklada z UI elementom
            plane.transform.Rotate(new Vector3(1, 0, 0), +90);
            plane.GetComponent<MeshRenderer>().enabled = false;
            plane.AddComponent<BoxCollider>();
            plane.GetComponent<BoxCollider>().isTrigger = true;
            plane.AddComponent<OpenDetailsInAR>();

            uiTextGO = new GameObject();
            uiTextGO.AddComponent<Canvas>(); // uiTextGO spremenimo v Canvas
            uiTextGO.AddComponent<OpenDetailsInAR>();
            uiTextGO.AddComponent<GraphicRaycaster>();

            // Transformacija uiText game objekta glede na plane
            uiTextGO.transform.SetParent(plane.transform);
            var ls = uiTextGO.transform.localScale;
            uiTextGO.transform.localScale = new Vector3(ls.x * -1, ls.y, ls.z);

            // Dodamo SightPrefab na canvas in ustrezno transformiramo
            sightPrefab = UnityEngine.Object.Instantiate(Resources.Load<GameObject>("SightPrefab_Large"));
            sightPrefab.transform.SetParent(uiTextGO.transform);
            uiTextGO.transform.localScale = new Vector3(ls.x / 10.7018f, ls.y / 11.7726f, ls.z);
            sightPrefab.transform.position = uiTextGO.transform.position;
        }

        public void setDistanceText(int distanceMeters, string name, string address, Sprite category)
        {
            string distanceMetersDisplay = distanceMeters + " m";

            if (distanceMeters >= 1000)
                distanceMetersDisplay = (distanceMeters / 1000) + " km"; //+ "," + (distanceMeters % 1000).ToString().PadRight(3, '0') + " km";

            // Dobimo game objekte prefab-a za 
            GameObject bg_active = sightPrefab.transform.GetChild(0).transform.gameObject;
            Image sight_icon = bg_active.transform.GetChild(SIGHT_ICON).transform.gameObject.GetComponent<Image>();
            Text sight_name = bg_active.transform.GetChild(SIGHT_NAME).transform.gameObject.GetComponent<Text>();
            Text sight_address = bg_active.transform.GetChild(SIGHT_ADDRESS).transform.gameObject.GetComponent<Text>();
            Text sight_distance = bg_active.transform.GetChild(SIGHT_DISTANCE).transform.gameObject.GetComponent<Text>();

            sight_icon.sprite = category;
            sight_name.text = name;
            sight_address.text = address;
            sight_distance.text = distanceMetersDisplay;
        }

        public void SetParent(GameObject parent)
        {
            plane.transform.parent = parent.transform;
        }

        public void update()
        {
            plane.transform.localPosition = new Vector3(0, 4, 0); // Vertikalna razdalja tablice od tal (parent 3D objekta (sphere))
            plane.transform.LookAt(User.camera.transform);
            plane.transform.Rotate(new Vector3(1, 0, 0), 90.0f);
        }
    }
}
